package main.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.app.MainApp;

public class DatabaseManager {
	private static final String DB_DRIVER_PATH = "com.mysql.cj.jdbc.Driver";
	private static final String DB_NAME = "adriankj_TDT4145";
	private static final String DB_USERNAME = "adriankj_demo";
	private static final String DB_PASSWORD = "adriankj_demo";
	private static final String CONNECTION_STRING = "jdbc:mysql://mysql.stud.ntnu.no/" + DB_NAME + "?serverTimezone=UTC";
	
	private static Connection connection;
	
	
	/*
	 * Setting up a connection to the database
	 */
	static {
		openConnection();
	}
	
	
	/**
	 * Returns a new {@link PreparedStatement} representing the {@code statement} specified.
	 */
	public static PreparedStatement getPreparedStatement(String statement) {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(statement);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return preparedStatement;
	}
	
	/*
	 * Sends query
	 */
	public static ResultSet executeQuery(PreparedStatement preparedStatement) {
		ResultSet resultSet = null;
		try {
			resultSet = preparedStatement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet;
	}

	
	/*
	 * Sends query 
	 */
	public static ResultSet executeQuery(String query) {
		return executeQuery(getPreparedStatement(query));
		
	}
	
	/*
	 * Sends update and reestablishes connection if lost.
	 */	
	public static int executeUpdate(PreparedStatement preparedStatement) {
		try {
			preparedStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	/*
	 * Sends update and reestablishes connection if lost.
	 */
	public static int executeUpdate(String update) {
		return executeUpdate(getPreparedStatement(update));
	}
	
		
	/*
	 * Opens SQL connection.
	 * Runs at launch, and if the connection times out etc. 
	 */
	private static void openConnection() {
		try {
			Class.forName(DB_DRIVER_PATH);
			connection = DriverManager.getConnection(CONNECTION_STRING, DB_USERNAME, DB_PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("No connection ");
		}
	}
	
	/*
	 * Closes SQL connection.
	 * Runs from shutdown hook. 
	 */
	private static void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	public static void main(String[] args) {
		ResultSet resultSet = executeQuery("SELECT * FROM apparat");	
		}
		
		
	
}
