package main.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import main.models.Notat;

public class NotatQueryHandler {
	
//	public static Notat getNotatByID(int notatID) {
//		try {
//			String query = "SELECT * FROM notat WHERE notatID = ?";
//			PreparedStatement preparedStatement = DatabaseManager.getPreparedStatement(query);
//			preparedStatement.setInt(1, notatID);
//			
//			ResultSet resultSet = DatabaseManager.executeQuery(preparedStatement);
//			return extractNotatFromResultSet(resultSet);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

	public static Notat getNotatByID(int notatID) {
		String query = String.format("SELECT * FROM notat WHERE notatID = %s;", notatID);
		return extractNotatFromResultSet(DatabaseManager.executeQuery(query));
	}
	
	
	public static Notat extractNotatFromResultSet(ResultSet resultSet) {
		Notat notat = null;
		try {
			int notatID = resultSet.getInt("notatID");
			String tekst = resultSet.getString("tekst");
			notat = new Notat(notatID, tekst);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return notat;
	}
}
