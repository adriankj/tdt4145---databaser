package main.app;



import java.sql.ResultSet;
import java.sql.SQLException;

import main.database.DatabaseManager;

public class MainApp {
	public static void main(String[] args) {
		
		ResultSet resultSet = DatabaseManager.executeQuery("SELECT * FROM Notat");
		DatabaseManager.executeUpdate("INSERT INTO Notat values(3,'mitt tredje notat')");
		try {
			resultSet.next();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String tekst = null;
		int notatID = 0;
		try {
			notatID = resultSet.getInt("NotatID");
			tekst = resultSet.getString("Tekst");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println(String.format("%s %s" ,notatID, tekst));
	}
}
